#!/bin/bash

APPS_HOME="/c/apps"


cat <<EOF > $HOME/.inputrc
# ~/.inputrc
set colored-stats On
set completion-ignore-case On
set completion-prefix-display-length 3
set mark-symlinked-directories On
set show-all-if-ambiguous On
set show-all-if-unmodified On
set visible-stats On
EOF


cat <<EOF > $HOME/.profile
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
# include .bashrc if it exists
if [ -f "$HOME/.bashrc" ]; then
. "$HOME/.bashrc"
fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
PATH="$HOME/.local/bin:$PATH"
fi


PATH="${PATH}:${APPS_HOME}/R/R-4.3.2/bin/x64"
PATH="${PATH}:${APPS_HOME}/rtools43/usr/bin"
PATH="${PATH}:${APPS_HOME}/RStudio"
PATH="${PATH}:${APPS_HOME}/RStudio/resources/app/bin/postback"
PATH="${PATH}:${APPS_HOME}/RStudio/resources/app/bin/quarto/bin"
PATH="${PATH}:${APPS_HOME}/sublime"
PATH="${PATH}:${APPS_HOME}/Meld"
EOF


#
# Install
#
pacman -Sy --needed --noconfirm base
pacman -Sy --needed --noconfirm base-devel
pacman -Sy --needed --noconfirm tree
pacman -Sy --needed --noconfirm wget
pacman -Sy --needed --noconfirm cmake
pacman -Sy --needed --noconfirm openssh
pacman -Sy --needed --noconfirm git


#
# Setup ssh
#
cd $HOME
mkdir $HOME/.ssh
ssh-keygen -t rsa -q -f "$HOME/.ssh/id_rsa" -N ""
echo "" >> $HOME/.ssh/config
